function flatArray (arr) {
  return arr.flat();
}

function aggregateArray (arr) {
  const aggregateArray = [];
  for (let i = 0; i < arr.length; i++) {
    aggregateArray.push([arr.map(element => element * 2)[i]]);
  }
  return aggregateArray;
}

function getEnumerableProperties (obj) {
  const props = [];
  for (const prop in obj) {
    props.push(prop);
  }
  return props;
}

function removeDuplicateItems (arr) {
  const removeDuplicateItems = [...new Set(arr)];
  return removeDuplicateItems;
}

function removeDuplicateChar (str) {
  let removeDuplicateChar = '';
  for (let i = 0; i < str.length; i++) {
    if (removeDuplicateChar.search(str[i]) === -1) {
      removeDuplicateChar += str[i];
    }
  }
  return removeDuplicateChar;
}

function addItemToSet (set, item) {
  set.add(item);
  return set;
}

function removeItemToSet (set, item) {
  set.delete(item);
  return set;
}

function countItems (arr) {
  const countItems = new Map();
  new Set(arr).forEach(element => {
    const count = arr.filter(item => item === element).length;
    countItems.set(element, count);
  });
  return countItems;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};

function joinArrays (arr1, arr2) {
  return arr1.concat(arr2);
}

function checkAdult (arr, age) {
  const arrJudgement = arr.map(element => element >= age);
  if (arrJudgement.includes(false)) {
    return false;
  }
  return true;
}

function findAdult (arr, age) {
  const arrOfAdults = arr.filter(element => element >= age);
  return arrOfAdults[0];
}

function convertArrToStr (arr, str) {
  return arr.join(str);
}

function removeLastEle (arr) {
  arr.pop();
  return arr;
}

function addNewItem (arr, item) {
  arr.push(item);
  return arr;
}

function removeFirstItem (arr) {
  arr.shift();
  return arr;
}

function addNewItemToBeginArr (arr, item) {
  arr.unshift(item);
  return arr;
}

function reverseOrder (arr) {
  arr.reverse();
  return arr;
}

function selectElements (arr, start, end) {
  return arr.slice(start, end);
}

function addItemsToArray (arr, index, howmany, item) {
  arr.splice(index, howmany, item);
  return arr;
}

function sortASC (arr) {
  arr.sort((a, b) => a - b);
  return arr;
}

function sortDESC (arr) {
  arr.sort((a, b) => b - a);
  return arr;
}

export {
  joinArrays,
  checkAdult,
  findAdult,
  convertArrToStr,
  removeLastEle,
  addNewItem,
  removeFirstItem,
  addNewItemToBeginArr,
  reverseOrder,
  selectElements,
  addItemsToArray,
  sortASC,
  sortDESC
};

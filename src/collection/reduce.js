function getMaxNumber (collection) {
  const getMaxNumber = collection.reduce((a, b) => a > b ? a : b);
  return getMaxNumber;
}

function isSameCollection (collection1, collection2) {
  if (collection1.toString() === collection2.toString()) {
    return true;
  }
  return false;
}

function sum (collection) {
  const sum = collection.reduce((a, b) => a + b);
  return sum;
}

function computeAverage (collection) {
  const sum = collection.reduce((a, b) => a + b);
  const averageValue = sum / collection.length;
  return averageValue;
}

function lastEven (collection) {
  const collectionOfEvens = collection.filter(element => element % 2 === 0);
  const lastEven = collectionOfEvens[collectionOfEvens.length - 1];
  return lastEven;
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };

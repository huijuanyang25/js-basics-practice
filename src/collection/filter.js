function getAllEvens (collection) {
  const collectionWithAllEvens = collection.filter(element => element % 2 === 0);
  return collectionWithAllEvens;
}

function getAllIncrementEvens (start, end) {
  const getAllIncrementEvens = [];
  for (let i = start; i <= end; i++) {
    if (i % 2 === 0) {
      getAllIncrementEvens.push(i);
    }
  }
  return getAllIncrementEvens;
}

function getIntersectionOfcollections (collection1, collection2) {
  const getIntersectionOfcollections = [];
  for (let i = 0; i < collection1.length; i++) {
    if (collection2.includes(collection1[i])) {
      getIntersectionOfcollections.push(collection1[i]);
    }
  }
  return getIntersectionOfcollections;
}

function getUnionOfcollections (collection1, collection2) {
  const concatTwoCollections = collection1.concat(collection2);
  const getUnionOfcollections = concatTwoCollections.filter((element, index) => concatTwoCollections.indexOf(element) === index);
  return getUnionOfcollections;
}

function countItems (collection) {
  const countItems = {};
  new Set(collection).forEach(element => {
    countItems[element] = collection.filter(item => item === element).length;
  });
  return countItems;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};

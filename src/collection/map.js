function doubleItem (collection) {
  const doubleItem = collection.map(element => element * 2);
  return doubleItem;
}

function doubleEvenItem (collection) {
  const doubleEvenItem = collection.filter(element => element % 2 === 0).map(item => item * 2);
  return doubleEvenItem;
}

function covertToCharArray (collection) {
  const covertToCharArray = collection.map(element => String.fromCharCode(element + 96));
  return covertToCharArray;
}

function getOneClassScoreByASC (collection) {
  const getOneClassScoreByASC = collection.filter(element => element.class === 1).map(item => item.score).sort();
  return getOneClassScoreByASC;
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };

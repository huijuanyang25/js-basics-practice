function getDayOfMonth (dateStr) {
  const date = new Date(dateStr);
  return date.getDate();
}

function getDayOfWeek (dateStr) {
  const date = new Date(dateStr);
  return date.getDay();
}

function getYear (dateStr) {
  const date = new Date(dateStr);
  return date.getFullYear();
}

function getMonth (dateStr) {
  const date = new Date(dateStr);
  return date.getMonth();
}

function getMilliseconds (dateStr) {
  const date = new Date(dateStr);
  return date.getTime();
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
